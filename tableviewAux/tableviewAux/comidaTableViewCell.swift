//
//  comidaTableViewCell.swift
//  tableviewAux
//
//  Created by MacBook on 3/22/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class comidaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var miImagen: UIImageView!
    
    @IBOutlet weak var nombre: UILabel!
    
    @IBOutlet weak var detalle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
