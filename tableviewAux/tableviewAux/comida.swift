//
//  comida.swift
//  tableviewAux
//
//  Created by MacBook on 3/22/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

struct Comida {
    var nombre: String
    var detalle: String
    var nombreImagen: String
}
