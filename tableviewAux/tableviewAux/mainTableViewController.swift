//
//  mainTableViewController.swift
//  tableviewAux
//
//  Created by MacBook on 3/22/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class mainTableViewController: UITableViewController {
    
    var misDatos = [Comida(nombre: "Sopa", detalle: "Alimento caliente", nombreImagen: "Sopita"), Comida(nombre: "Alitas", detalle: "Para la fiesta", nombreImagen: "Alitas"), Comida(nombre: "Marucha", detalle: "Que rica", nombreImagen: "Plastico")]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return misDatos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "miCeldita", for: indexPath) as! comidaTableViewCell
        
        cell.nombre.text = misDatos[indexPath.row].nombre
        cell.detalle.text = misDatos[indexPath.row].detalle
        cell.miImagen.image = UIImage(named: misDatos[indexPath.row].nombreImagen)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }


    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segundaVista = segue.destination as! segundoViewController
        
        let indexPath = tableView.indexPathForSelectedRow!
        
        segundaVista.nombre = misDatos[indexPath.row].nombre
    }

}
