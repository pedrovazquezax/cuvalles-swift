//
//  MyAnnotation.swift
//  MapKitDemo
//
//  Created by L Daniel De San Pedro on 3/22/19.
//  Copyright © 2019 L Daniel De San Pedro. All rights reserved.
//

import Foundation
import MapKit

class MyAnnotation: NSObject, MKAnnotation {
    
    var title: String?
    var coordinate: CLLocationCoordinate2D
    
    
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
    
    
}
